const mongoose = require("mongoose"),
	Post = require("./models/post"),
	User = require("./models/user"),
	Comment = require("./models/comment");
const data = [
	{
		title: "Lorem 1",
		body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis" +
		"lacus tortor, blandit porttitor neque id, placerat facilisis erat. " +
		"Ut suscipit velit non odio efficitur, vel tempor nunc gravida. Duis " +
		"nisi mauris, pulvinar vel porttitor eu, volutpat id dui. Pellentesque"+ 
		"molestie in dui et mollis. Vivamus ac malesuada tortor, a vestibulum "+
		"sem. Etiam non dapibus turpis. In hac habitasse platea dictumst. Cras "+
		"nec erat nibh. Donec iaculis felis sed tristique ultricies. Vestibulum "+
		"porttitor nec metus a suscipit. Praesent tristique metus a magna "+
		"suscipit, vel luctus turpis molestie. Maecenas lobortis ullamcorper "+ 
		"efficitur.",
		image: "http://i.ysi.bz/Assets/73/034/L_p0014503473.jpg"
	},
	{
		title: "Lorem the Second",
		body: "11111111111111111111111111 elit. Duis" +
		"lacus tortor, blandit porttitor neque id, placerat facilisis erat. " +
		"Ut suscipit velit non odio efficitur, vel tempor nunc gravida. Duis " +
		"nisi mauris, pulvinar vel porttitor eu, volutpat id dui. Pellentesque"+ 
		"molestie in dui et mollis. Vivamus ac malesuada tortor, a vestibulum "+
		"sem. Etiam non dapibus turpis. In hac habitasse platea dictumst. Cras "+
		"nec erat nibh. Donec iaculis felis sed tristique ultricies. Vestibulum "+
		"porttitor nec metus a suscipit. Praesent tristique metus a magna "+
		"suscipit, vel luctus turpis molestie. Maecenas lobortis ullamcorper "+ 
		"efficitur.",
		image: "http://i.ysi.bz/Assets/18/308/M_p0058130818.jpg"
	},
	{
		title: "Lorem the Third",
		body: "222222222222222222222222222 elit. Duis" +
		"lacus tortor, blandit porttitor neque id, placerat facilisis erat. " +
		"Ut suscipit velit non odio efficitur, vel tempor nunc gravida. Duis " +
		"nisi mauris, pulvinar vel porttitor eu, volutpat id dui. Pellentesque"+ 
		"molestie in dui et mollis. Vivamus ac malesuada tortor, a vestibulum "+
		"sem. Etiam non dapibus turpis. In hac habitasse platea dictumst. Cras "+
		"nec erat nibh. Donec iaculis felis sed tristique ultricies. Vestibulum "+
		"porttitor nec metus a suscipit. Praesent tristique metus a magna "+
		"suscipit, vel luctus turpis molestie. Maecenas lobortis ullamcorper "+ 
		"efficitur.",
		image: "http://i.ysi.bz/Assets/63/581/M_p0058258163.jpg"
	},
];
function seedDB() {
	// remove all posts
	Post.remove({}, function(err) {
		if (err) {
			console.log(err);
		}
		console.log("Posts were removed");
		// add new posts
		data.forEach(function(post) {
			Post.create(post, function(err, newPost) {
				if (err) {
					console.log(err);
				} else {
					console.log("New post added");
					// create a comment
					Comment.create({
						text: "hi guys",
						author: "homer"
					}, function(err, comment) {
						if (err) {
							console.log(err);
						} else {
							newPost.comments.push(comment);
							newPost.save();
							console.log("created a new comment");
						}
					});
					Comment.create({
						text: "foobar",
						author: "jim"
					}, function(err, comment) {
						if (err) {
							console.log(err);
						} else {
							newPost.comments.push(comment);
							newPost.save();
							console.log("created a new comment");
						}
					});
				}
			});
		});
	});
	// remove all users
	User.remove({}, function(err) {
		if (err) {
			console.log(err);
		}
	});
}
module.exports = seedDB;