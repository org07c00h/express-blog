var mongoose = require("mongoose"),
	passportLocalMongoose = require("passport-local-mongoose");
var UserSchema = new mongoose.Schema({
	username: String,
	password: String
});
// Add methods from passport-local-mongoose
UserSchema.plugin(passportLocalMongoose);
module.exports = mongoose.model("User", UserSchema);