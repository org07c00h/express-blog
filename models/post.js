var mongoose = require("mongoose");
//Comment = require("./comment");
var postSchema = new mongoose.Schema({
	title: String,
	image: {
		type: String,
		default: "/images/no-image.png"
	},
	body: String,
	markdown: {
		type: String,
		trim: true
	},
	comments: [{
		type: mongoose.Schema.Types.ObjectId,
		ref: "Comment"
	}],
	author: {
		id: {
			type: mongoose.Schema.Types.ObjectId,
			ref: "User"
		},
		username: String
	},
	tags: [String],	
	created: {type: Date, default: Date.now}
});
module.exports = mongoose.model("Post", postSchema);