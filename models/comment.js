var mongoose = require("mongoose");
var commentShema = new mongoose.Schema({
	author: String,
	text: String,
	created: {type: Date, default: Date.now}
});
module.exports = mongoose.model("Comment", commentShema);