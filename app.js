var express = require("express"),
	passport = require("passport"),
	LocalStrategy = require("passport-local"),
	passportLocalMongoose = require("passport-local-mongoose"),
	bodyParser = require("body-parser"),
	mongoose = require("mongoose"),
	methodOverride = require("method-override"),
	expressSanitizer = require("express-sanitizer"),
	fileUpload = require("express-fileupload"),
	app = express();
// require routes
var postRoutes = require("./routes/posts"),
	commentRoutes = require("./routes/comments"),
	tagRoutes = require("./routes/tags"),
	fileRoutes = require("./routes/files"),
	indexRoutes = require("./routes/index");
// let's initialize everyting
mongoose.Promise = global.Promise;
mongoose.connect("mongodb://localhost/Post_app");
app.set("view engine", "ejs");
app.use(express.static("public"));
//app.use(fileUpload());
app.use(bodyParser.urlencoded({extended: true}))
app.use(methodOverride("_method"));
app.use(expressSanitizer());
// init sessions
app.use(require("express-session")({
	secret: "Close the world. txen eht nepO",
	resave: false,
	saveUninitialized: false
}));
// schemas
var Comment = require("./models/comment"),
	User = require("./models/user"),
	Post = require("./models/post");
// init passport for authentication
app.use(passport.initialize());
app.use(passport.session());
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());
// check if user logged in
app.use(function(req, res, next) {
	res.locals.currentUser = req.user;
	next();
});
// seed the db
// seedDB();
// init routes
app.use("/", indexRoutes);
app.use("/posts", postRoutes);
app.use("/posts/tags", tagRoutes);
app.use("/files", fileRoutes);
app.use(commentRoutes);
const ip = "0.0.0.0",
	port = 8080;
app.listen(port, ip, function(){
	console.log("Server is running");
});