var express = require("express"),
	User = require("../models/user"),
	passport = require("passport"),	
	isLoggedIn = require("../checkLoggedIn"),
	router = express.Router();
router.get("/", function(req, res) {
	res.redirect("/posts");
});
router.get("/secret", isLoggedIn, function(req, res) {
	res.send("this is the secret page");
});
// show sign up form
router.get("/signup", function(req, res) {
	res.render("signup");
});
// handling user sign up
router.post("/signup", function(req, res) {
	User.register(new User({username: req.body.username}),
		req.body.password, function(err, user) {
			if (err) {
				console.log(err);
				return res.render('signup');
			}
			console.log("New user was added to DB");
			passport.authenticate("local")(req, res, function() {
				res.redirect("/");
			});
		});
});
// show sign in form
router.get("/signin", function(req, res) {
	res.render("signin");
});
// sign in login
router.post("/signin", passport.authenticate("local", {
	successRedirect: "/",
	failureRedirect: "/signin"
}), function(req, res) {
});
// logout
router.get("/logout", function(req, res) {
	req.logout();
	res.redirect("/");
});
module.exports = router;
