var express = require("express"),
	Post = require("../models/post")
	router = express.Router(),
	isLoggedIn = require("../checkLoggedIn"),
	multer = require("multer"),
	fs = require("fs"),
	markdown = require("markdown").markdown;
	var upload = multer({
	  dest: __dirname + '/../public/upload/temp'
	});
// index route
router.get("/", function(req, res) {
	Post.find({}).sort({created: -1}).exec(function(err, posts) {
		if (err) {
			console.log("error");
		} else {
			res.render("index", {posts: posts});
		}
	});
});
// new route
router.get("/new", isLoggedIn, function(req, res) {
	res.render("new");
});
// create route
router.post("/", isLoggedIn, upload.single('post[imageFile]'), function(req, res) {
	// create Post
	req.body.post.body = markdown.toHTML(req.body.post.markdown);
	let author = {
		id: req.user._id,
		username: req.user.username
	};
	let image;
	let tags;
	if (req.body.post.tags.length == 0) {
		tags = ["uncategorized"];
	} else {
		// let's make uniq tags
		let splittedTags = req.body.post.tags.split(",");
		for (let i = 0; i < splittedTags.length; i++) {
			splittedTags[i] = splittedTags[i].trim();
		}
		let tagSet = new Set(splittedTags);
		tags = Array.from(tagSet);
	}
	let newPost = {
		title: req.body.post.title,
		body: req.body.post.body,
		markdown: req.body.post.markdown,
		tags: tags,
		author: author
	};
	let file = req.file;
	if (!req.file) {
		newPost.image = req.body.post.imageUrl;
		createPost(res, newPost);
	} else {
		let ending;
		switch (file.mimetype) {
			case "image/jpeg":
				ending = "jpg";
			break;
			case "image/png":
				ending = "png";
			break;
			case "image/gif":
				ending = "gif";
			break;
			default:
				createPost(res, newPost);
			break;
		}
		fs.rename(file.path, file.path + "." + ending, function(err) {
			if (err) {
				console.log(err);
			} else {
				newPost.image = "/upload/temp/" + file.filename + "." + ending;
				createPost(res, newPost);
			}
		});
	}
});
// show route
router.get("/:id", function(req, res) {
	Post.findById(req.params.id).populate("comments").exec(function(err, foundPost) {
		if (err) {
			res.redirect("/");
		} else {
			console.log(foundPost);
			if (foundPost == null) {
				res.redirect("/");	
			}
			res.render("show", {post: foundPost});
		}
	});
});
// edit route
router.get("/:id/edit", isLoggedIn, function(req, res) {
	Post.findById(req.params.id, function(err, foundPost) {
		if (err) {
			res.redirect("/");
		} else {
			res.render("edit", {post: foundPost});
		}
	});
});
// update route
router.put("/:id", isLoggedIn, function(req, res) {
	req.body.post.markdown.trim();
	req.body.post.body = markdown.toHTML(req.body.post.markdown);
	let splittedTags = req.body.post.tags.split(",");
	for (let i = 0; i < splittedTags.length; i++) {
		splittedTags[i] = splittedTags[i].trim();
	}
	let tagSet = new Set(splittedTags);
	req.body.post.tags = Array.from(tagSet);
	Post.findByIdAndUpdate(req.params.id, req.body.post, function(err, updatedPost) {
		if (err) {
			res.redirect("/");
		} else {
			res.redirect("/posts/" + updatedPost._id);
		}
	})
});
// delete route
router.delete("/:id", isLoggedIn, function(req, res) {
	Post.findByIdAndRemove(req.params.id, function(err) {
		res.redirect("/");
	});
})
function createPost(res, newPost) {
	Post.create(newPost, function(err, newPost) {
		if (err) {
			res.render("new");
		} else {
			res.redirect("/");
		}
	});
}
module.exports = router;