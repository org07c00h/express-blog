var express = require("express"),
	Post = require("../models/post"),
	Comment = require("../models/comment"),
	router = express.Router();
// COMMENT ROUTES
router.post("/posts/:id/comments", function(req, res) {
	Post.findById(req.params.id, function(err, foundPost) {
		if (err) {
			res.redirect("/");
		} else {
			console.log("post found");
			Comment.create(req.body.comment, function(err, comment) {
				if (err) {
					res.redirect("/");
				} else {
					console.log("comment added");
					foundPost.comments.push(comment);
					foundPost.save();
					res.redirect("/posts/" + foundPost._id);
				}
			});
		}
	});
});
module.exports = router;