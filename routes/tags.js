var express = require("express"),
	Post = require("../models/post")
	router = express.Router();
router.get("/:tag", function(req, res) {
	Post.find({tags: req.params.tag}).sort({created: -1}).exec(function(err, posts) {
		if (err) {
			console.log("error");
		} else {
			res.render("index", {posts: posts});
		}
	});
});
module.exports = router;