var express = require("express"),
	router = express.Router(),
	isLoggedIn = require("../checkLoggedIn"),
	multer = require("multer"),
	fs = require("fs"),
	File = require("../models/file"),
	upload = multer({
	  dest: __dirname + '/../public/upload/temp'
	});
// TODO: Add isLoggedIn
// show all uploaded files
router.get("/", isLoggedIn, function(req, res) {
	File.find({}).populate('user').sort({created: -1}).exec(function(err, files) {
		if (err) {
			console.log("error");
		} else {
			res.render("files/show", {files: files});
		}
	});
});
// show upload form
router.get("/new", function(req, res) {
	res.render("files/new");
});
// upload a file
router.post("/", upload.array('file'),(req, res) => {
	let files = req.files;
	let returnData = { location: [] };
	if (files) {
		files.forEach(function(file) {
			let extention = file.originalname.split('.').pop();	
			returnData.location.push("/upload/temp/" + file.filename + "." + extention);
			fs.rename(file.path, file.path + "." + extention, function(err) {
				if (err) {
					console.log(err);
				} else {
					let location = "/upload/temp/" + file.filename + "." + extention;
					File.create({user: req.user._id, location: location}, function(err, file) {
						if (err) {
							res.redirect("/files/new");
						} else {
							// TODO: ?
						}
					});
				}
			});
		});
		res.send(returnData);
	} else {
		res.redirect("/files");
	}
});
// delete a file
router.delete("/:id", function(req, res) {

});
// I think that's it
module.exports = router;